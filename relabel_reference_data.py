from osgeo import ogr
import sys

print(sys.argv)

driver = ogr.GetDriverByName('ESRI Shapefile')
fn1 = sys.argv[1]
fn2 = sys.argv[2]
fieldName = sys.argv[3]

labelSet = set()

for fn in [fn1, fn2]:
    dataSource = driver.Open(fn, 0)
    layer = dataSource.GetLayer()
    feature = layer.GetNextFeature()

    while feature:
        labelSet.add(feature.GetField(fieldName))
        #layer.SetFeature(feature)
        feature = layer.GetNextFeature()

    dataSource.Destroy()

labelDict = {l:i for i,l in enumerate(sorted(labelSet))}

for fn in [fn1, fn2]:
    dataSource = driver.Open(fn, 1)
    layer = dataSource.GetLayer()
    feature = layer.GetNextFeature()

    while feature:
        label = int(feature.GetField(fieldName))
        newLabel = labelDict[label]
        feature.SetField(fieldName, newLabel)
        layer.SetFeature(feature)
        feature = layer.GetNextFeature()

    dataSource.Destroy()
