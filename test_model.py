from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

# Imports
import numpy as np
import tensorflow as tf
import numpy as np
import random
import os
import shutil
import sys
import time
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import f1_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.utils import shuffle
from sklearn.metrics import confusion_matrix

from tricks import *

tf.logging.set_verbosity(tf.logging.INFO)


#### Functions from the TF basic CNN example
def weight_variable(shape):
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)

def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)

def conv2d(x, W):
    return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')

def max_pool_2x2(x):
    return tf.nn.max_pool(x, ksize=[1, 2, 2, 1],
                          strides=[1, 2, 2, 1], padding='SAME')

def conv_layer(input, shape):
    W = weight_variable(shape)
    b = bias_variable([shape[3]])
    return tf.nn.relu(conv2d(input, W) + b)

def full_layer(input, size):
    in_size = int(input.get_shape()[1])
    W = weight_variable([in_size, size])
    b = bias_variable([size])
    return tf.matmul(input, W) + b

############################################
def main(unused_argv):
    """ Main function
    In this function we do:
    1. Import a dataset
    2. Build a model implementing a CNN
    3. Perform training of the CNN
    4. Export the model
    """

    ############################################################
    #                     import a dataset 
    ############################################################  
  
    # check number of arguments
    if len(sys.argv) < 4:
        print("Usage : <patches> <labels> <output_model_dir> [<epochs>]")
        sys.exit(1)
  
    # Export dir
    log_dir = sys.argv[3] + '/model_checkpoints/'
    export_dir = sys.argv[3] + '/model_export/'
  
    print("loading dataset")
  
    # Create a dataset of size imp_size
    imp_ds_patches = read_samples(sys.argv[1])
    imp_ds_labels  = read_samples(sys.argv[2])
    
    # Shuffle the dataset
    imp_ds_patches,imp_ds_labels = shuffle(imp_ds_patches,imp_ds_labels, random_state=0)
    
    print("ok")
    
     # Number of samples
    if (imp_ds_patches.shape[0] != imp_ds_labels.shape[0]):
        print("Number of samples should be the same as number of patches!")
        sys.exit(1)
  
  
    nb_samples = imp_ds_patches.shape[0]
  
    # Training params
    n_epoch = 50
    batch_size = 32
    learning_rate = 0.0001
  
    if len(sys.argv) > 4:
        n_epoch = int(sys.argv[4])
  
    print('Training with %s epochs' %(n_epoch))
  
    # Number of samples for training  
    n_data_train = int(imp_ds_patches.shape[0] / 2)
  
    # Size of patches  
    nb_bands = imp_ds_patches.shape[3]
    patch_size_xs = imp_ds_patches.shape[1]
    patch_size_label = 1
    nbClasses = 13
  
    ds_data_train = imp_ds_patches[0:n_data_train,:]
    ds_data_valid = imp_ds_patches[n_data_train:imp_ds_patches.shape[0],:]
    ds_labels_train = imp_ds_labels[0:n_data_train,:]
    ds_labels_valid = imp_ds_labels[n_data_train:imp_ds_patches.shape[0],:]
  
    print('ds_data_train shape %s'%str(ds_data_train.shape))
  
    ############################################################
    #                    Build the graph
    ############################################################
    
    with tf.Graph().as_default():
  
      # placeholder for images and labels
      istraining_placeholder = tf.placeholder(tf.bool, shape=(), name="istraining") # Used only for dropout...
      xs_placeholder = tf.placeholder(tf.float32, shape=(None, patch_size_xs,patch_size_xs,nb_bands), name="x1")
      labels_placeholder = tf.placeholder(tf.int32, shape=(None, 1, 1, 1), name="y1")
      
      print_tensor_info("xs_placeholder",xs_placeholder)
      print_tensor_info("labels_placeholder",labels_placeholder)
      
  
      ##### CNN model
      kernel_size = 3
      nbFilters = 32
      nbFilters2 = 32
      conv1 = conv_layer(xs_placeholder, shape=[kernel_size, kernel_size, nb_bands, nbFilters])
      pool1 = max_pool_2x2(conv1)
      patch_size_after_pool = int(patch_size_xs/2)
  
      # conv2 = conv_layer(pool1, shape=[kernel_size, kernel_size, nbFilters, nbFilters2])
      # pool2 = max_pool_2x2(conv2)
      # patch_size_after_pool = int(patch_size_after_pool/2)
  
  
      nbFiltersEnd = nbFilters
      nb_inputs_flat = patch_size_after_pool*patch_size_after_pool*nbFiltersEnd
  
      conv_flat = tf.reshape(pool1, [-1, nb_inputs_flat])
  
      full_1 = tf.nn.relu(full_layer(conv_flat, 1024))
      y_pred = full_layer(full_1, nbClasses)
  
      testPrediction = tf.argmax(y_pred, 1, name="prediction")    
  
      loss = tf.losses.sparse_softmax_cross_entropy(labels=tf.reshape(labels_placeholder, [-1, 1]), logits=tf.reshape(y_pred, [-1, nbClasses]))
      train_op = tf.train.AdamOptimizer(1e-4, name="optimizer").minimize(loss)
  
      
      ############### Variable initializer Op ##################
  
      init = tf.global_variables_initializer()
      
      ######################### Saver ##########################
  
      saver = tf.train.Saver()
      
      #################### Create a session ####################
      
      sess = tf.Session()
      sess.run(init)
  
      ############## Here we start the training ################
      
      for curr_epoch in range(n_epoch):
  
        print("Epoch #" + str(curr_epoch))
        
        ds_data_train,ds_labels_train = shuffle(ds_data_train,ds_labels_train, random_state=0)
        
        # Start the training loop.
        n_steps = int(n_data_train / batch_size)
        for step in range(n_steps):
            
          start_time = time.time()
          
          # Fill a feed dictionary with the actual set of images and labels
          # for this particular training step.
          # Load training and eval data
          start_idx = batch_size * step
          end_idx = start_idx + batch_size 
          feed_dict = {
              xs_placeholder: ds_data_train[start_idx:end_idx,:],
              labels_placeholder: ds_labels_train[start_idx:end_idx,:],
              istraining_placeholder: True,
          }
          
          
          # Run one step of the model.  The return values are the activations
          # from the `train_op` (which is discarded) and the `loss` Op.  To
          # inspect the values of your Ops or variables, you may include them
          # in the list passed to sess.run() and the value tensors will be
          # returned in the tuple from the call.
          _, loss_value = sess.run([train_op, loss], feed_dict=feed_dict)
  
          duration = time.time() - start_time
  
          # Print an overview fairly often.
          if step % 100 == 0:
              # Print status to stdout.
            print('Step %d: loss = %.2f (%.3f sec)' % (step, loss_value, duration))
            #print('Step %d: (%.3f sec)' % (step, duration))
  
  
                  
      # Let's export a SavedModel
      shutil.rmtree(export_dir)
      builder = tf.saved_model.builder.SavedModelBuilder(export_dir)
      signature_def_map= {
          "model": tf.saved_model.signature_def_utils.predict_signature_def(
              inputs= {"x1": xs_placeholder},
              outputs= {"prediction": testPrediction})
      }
      builder.add_meta_graph_and_variables(sess, [tf.saved_model.tag_constants.TRAINING], signature_def_map)
      builder.add_meta_graph([tf.saved_model.tag_constants.SERVING])
      builder.save()
  
    quit()
    
if __name__ == "__main__":
      
    tf.add_check_numerics_ops()
    tf.app.run(main)
