#PBS -l select=1:ncpus=20:mem=100000mb
#PBS -N tf-tp-otb
#PBS -l walltime=200:00:00


export OLD_PATH=${PATH}
export OLD_LD_LIBRARY_PATH=${LD_LIBRARY_PATH}
export OLD_PYTHONPATH=$PYTHONPATH

export ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS=19
export OMP_NUM_THREADS=${ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS}
export OTB_CONFIG=/work/OT/theia/oso/jordi/OTB/otb_superbuild/otb_superbuild-otb_tf-Release-install/config_otb_tf_py35.sh
export OTB_TF_DIR=/work/OT/theia/oso/jordi/src/otb/Modules/External/otbtf
export OTB_TF_TEST_DIR=/work/OT/theia/oso/jordi/otb-dl-tests
export WS_DATA=/work/OT/theia/oso/jordi/WorkshopData
export WORKING_DIR=/work/OT/theia/oso/jordi/tmp
export IMAGE_DIR=${WS_DATA}/classification/images/
export SITSVRT=${IMAGE_DIR}/all.vrt
export SITS=${WORKING_DIR}/all.tif
export ORIG_TEST_DATA=${WS_DATA}/classification/references/testing/testing.*
export ORIG_TRAIN_DATA=${WS_DATA}/classification/references/training/training.*
export TEST_DATA=${WORKING_DIR}/testing.shp
export TRAIN_DATA=${WORKING_DIR}/training.shp
cp ${ORIG_TEST_DATA} ${WORKING_DIR}/
cp ${ORIG_TRAIN_DATA} ${WORKING_DIR}/
export MODEL_DIR=${WORKING_DIR}/saved_model
mkdir -p ${MODEL_DIR}/model_export
export NBSAMPLES=4000

# ----- source ${OTB_CONFIG}
module load gcc/6.3.0
module load python/3.5.2
export ITK_AUTOLOAD_PATH=""
export install_dir=/work/OT/theia/oso/jordi/OTB/otb_superbuild/otb_superbuild-otb_tf-Release-install
export PATH=$install_dir/bin:/work/OT/theia/oso/jordi/src/tensorflow_install/lib:/work/OT/theia/oso/jordi/src/tensorflow_install/bin:$PATH
export LD_LIBRARY_PATH=$install_dir/lib:$install_dir/lib/otb/python:/work/OT/theia/oso/jordi/src/tensorflow_install/lib:${LD_LIBRARY_PATH}
export OTB_APPLICATION_PATH=$install_dir/lib/otb/applications/:/work/OT/theia/oso/jordi/src/tensorflow_install/lib
export PYTHONPATH=$install_dir/lib/otb/python:$install_dir/lib/python3.5/site-packages/:$install_dir/lib64/python3.5/site-packages:$PYTHONPATH
export GDAL_DATA=$install_dir/share/gdal
export GEOTIFF_CSV=$install_dir/share/epsg_csv
# ----- 

# Relabel the reference data so we have continuous labels:

#+BEGIN_SRC bash 
python ${OTB_TF_TEST_DIR}/relabel_reference_data.py ${TEST_DATA} ${TRAIN_DATA} CODE
#+END_SRC

# Convert the input image to [0-1]

#+BEGIN_SRC bash
otbcli_Rescale -in ${SITSVRT} -out ${SITS} float -outmin 0 -outmax 1
#+END_SRC


#+BEGIN_SRC bash
otbcli_PolygonClassStatistics -vec ${TRAIN_DATA} -field CODE -in ${SITS} -out ${WORKING_DIR}/vec_stats.xml
otbcli_PolygonClassStatistics -vec ${TEST_DATA} -field CODE -in ${SITS} -out ${WORKING_DIR}/vec_stats_test.xml
#+END_SRC

#Then, we will select some samples with the SampleSelection application of the existing machine learning framework of OTB.

#+BEGIN_SRC bash
otbcli_SampleSelection -in ${SITS} -vec ${TRAIN_DATA} -instats ${WORKING_DIR}/vec_stats.xml -field CODE -out ${WORKING_DIR}/points.shp -strategy constant -strategy.constant.nb ${NBSAMPLES}
otbcli_SampleSelection -in ${SITS} -vec ${TEST_DATA} -instats ${WORKING_DIR}/vec_stats_test.xml -field CODE -out ${WORKING_DIR}/points_test.shp -strategy constant -strategy.constant.nb ${NBSAMPLES}
#+END_SRC

#Ok. Now, let's use our PatchesExtraction application. Out model has a perceptive field of 16x16 pixels. 
#We want to produce one image of patches, and one image for the corresponding labels.

#+BEGIN_SRC bash
# be careful as this needs lots of RAM
export OTB_TF_NSOURCES=1
otbcli_PatchesExtraction -source1.il ${SITS} -source1.patchsizex 16 -source1.patchsizey 16 -vec ${WORKING_DIR}/points.shp -field CODE -outlabels ${WORKING_DIR}/samp_labels.tif -source1.out ${WORKING_DIR}/samp_patches.tif
otbcli_PatchesExtraction -source1.il ${SITS} -source1.patchsizex 16 -source1.patchsizey 16 -vec ${WORKING_DIR}/points_test.shp -field CODE -outlabels ${WORKING_DIR}/samp_labels_test.tif -source1.out ${WORKING_DIR}/samp_patches_test.tif
#+END_SRC

#That's it. Now we have two images for patches and labels. If we wanna, we can split them to distinguish test/validation groups (with the ExtractROI application for instance). First we run a quick training in python and save the model.

#+BEGIN_SRC bash
export PATH=${OLD_PATH}
export LD_LIBRARY_PATH=${OLD_LD_LIBRARY_PATH}
export PYTHONPATH=${OLD_PYTHONPATH}
module swap python/2.7.12 python/3.5.2
module load python/3.5.2
module load openmpi/2.0.1
source /work/OT/siaa/Work/RTDLOSO/python3.5_virtualenv_tf_MKL/bin/activate
module load gcc/6.3.0
export ITK_AUTOLOAD_PATH=""
export install_dir=/work/OT/theia/oso/jordi/OTB/otb_superbuild/otb_superbuild-otb_tf-Release-install
export PATH=$install_dir/bin:$PATH
export LD_LIBRARY_PATH=$install_dir/lib:$install_dir/lib/otb/python:${LD_LIBRARY_PATH}
export OTB_APPLICATION_PATH=$install_dir/lib/otb/applications/
export PYTHONPATH=$install_dir/lib/otb/python:$install_dir/lib/python3.5/site-packages/:$install_dir/lib64/python3.5/site-packages:$PYTHONPATH
export GDAL_DATA=$install_dir/share/gdal
export GEOTIFF_CSV=$install_dir/share/epsg_csv
export PYTHONPATH=${OTB_TF_DIR}/python:${PYTHONPATH} #needed for tricks.py
python3 ${OTB_TF_TEST_DIR}/test_model.py ${WORKING_DIR}/samp_patches.tif ${WORKING_DIR}/samp_labels.tif ${MODEL_DIR}
deactivate
source ${OTB_CONFIG}
#+END_SRC

# Our model is quite basic. It has two input placeholders, x1 and y1 respectively for input patches (with size 16x16) and input reference labels (with size 1x1). We named prediction the tensor that predict the labels and the optimizer that perform the stochastic gradient descent is an operator named optimizer.

# Now we will perform some fine tuning of our model, located in the ~${MODEL_DIR}/export_model~ directory.  We perform the fine tuning and we export the new model variables. Let's use our TensorflowModelTrain application to perform the training of this existing model.

#+BEGIN_SRC bash
otbcli_TensorflowModelTrain -model.dir ${MODEL_DIR}/model_export -training.targetnodesnames optimizer -training.source1.il ${WORKING_DIR}/samp_patches.tif -training.source1.fovx 16 -training.source1.fovy 16 -training.source1.placeholder x1 -training.source2.il ${WORKING_DIR}/samp_labels.tif -training.source2.fovx 1 -training.source2.fovy 1 -training.source2.placeholder y1 -model.saveto ${MODEL_DIR}/variables
# segfault in validation
#-validation.mode class -validation.source1.il samp_patches_test.tif -validation.source1.placeholder x1 -validation.source2.il samp_labels_test.tif -validation.source2.placeholder prediction
#+END_SRC

# Note that we could also have performed validation in this step. In this case, the validation.source2.placeholder would be different than the training.source2.placeholder, and would be prediction. This way, the program know what is the target tensor to evaluate. 

# After this step, we decide to produce an entire map over the whole time series. First, we duplicate the model, and we replace its variable with the new ones that have been computed in the previous step.
# Then, we use the TensorflowModelServe application to produce the prediction tensor output for the entire image.

#+BEGIN_SRC bash
mv ${MODEL_DIR}/model_export/variables ${MODEL_DIR}/model_export/old_variables
mkdir ${MODEL_DIR}/model_export/variables 
mv ${MODEL_DIR}/variables* ${MODEL_DIR}/model_export/variables/
otbcli_TensorflowModelServe -source1.il ${SITS} -source1.placeholder x1 -source1.fovx 16 -source1.fovy 16 -model.dir ${MODEL_DIR}/model_export -output.names prediction -out ${TMPDIR}/map.tif uint8
mv ${TMPDIR}/map.tif ${WORKING_DIR}/map.tif
#+END_SRC


#Random Forests
#+BEGIN_SRC bash
otbcli_SampleExtraction -in ${SITS} -vec ${WORKING_DIR}/points.shp -out ${WORKING_DIR}/samples.sqlite -field CODE
export feats=`for i in $(seq 0 49); do printf "value_$i "; done`
otbcli_TrainVectorClassifier  -io.vd ${WORKING_DIR}/samples.sqlite -cfield code -io.out ${WORKING_DIR}/model.rf -classifier sharkrf -classifier.sharkrf.nbtrees 400 -feat ${feats}
otbcli_ImageClassifier -in ${SITS} -model ${WORKING_DIR}/model.rf -out ${WORKING_DIR}/maprf.tif uint8
otbcli_ClassificationMapRegularization -io.in ${WORKING_DIR}/maprf.tif -io.out ${WORKING_DIR}/maprfreg.tif
#+END_SRC

#Validation comparison

#+BEGIN_SRC bash :results output
otbcli_ComputeConfusionMatrix -in ${WORKING_DIR}/map.tif -out ${WORKING_DIR}/mc.csv -ref vector -ref.vector.in ${WORKING_DIR}/testing.shp -ref.vector.field CODE 
otbcli_ComputeConfusionMatrix -in ${WORKING_DIR}/maprf.tif -out ${WORKING_DIR}/mcrf.csv -ref vector -ref.vector.in ${WORKING_DIR}/testing.shp -ref.vector.field CODE 
otbcli_ComputeConfusionMatrix -in ${WORKING_DIR}/maprfreg.tif -out ${WORKING_DIR}/mcrfreg.csv -ref vector -ref.vector.in ${WORKING_DIR}/testing.shp -ref.vector.field CODE 
#+END_SRC
